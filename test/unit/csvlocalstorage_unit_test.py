from src.csvlocalstorage import CSVLocalStorage

import unittest



class CsvLocalStorage_UnitTest(unittest.TestCase):

    def test_getItem_EmptyStore_UponInitialization(self):
        store = CSVLocalStorage()

        self.assertTrue(not store.get_items())

    def test_putItem_SingleNonDictItem(self):
        store = CSVLocalStorage()

        store.put_item("foobar")

        self.assertTrue(not store.get_items())
    
    def test_putItem_SingleInvalidDictItem(self):
        store = CSVLocalStorage("tasks")

        store.put_item({"foo": "bar"})

        self.assertTrue(not store.get_items())

    def test_getItem_NonEmpty(self):
        store = CSVLocalStorage("tasks")

        item = {"tasks": "foobar"}
        store.put_item(item)

        with self.subTest():
            self.assertEqual(len(store.get_items()), 1, "Only one item was put into the store.")
        with self.subTest():
            self.assertEqual(store.get_items()[0], item, "Single item from get should be single item from put.")
    
    def test_getSingleItem_UsingSingleTag(self):
        store = CSVLocalStorage("tasks", "tag_field")

        tag = "Bar"
        item = {"tasks": "foobar", "tag_field": tag}
        store.put_item(item)
        store.put_item({"tasks": "foobaz", "tag_field": "Baz"})

        get_items = store.get_items({"tag_field": tag})
        with self.subTest():
            self.assertEqual(len(get_items), 1, "Only one item should have matched tag in tag field.")
        with self.subTest():
            self.assertEqual(get_items[0], item, "Item from get should be item with matching tag in tag field")

    def test_getItem_UsingInvalidTags(self):
        store = CSVLocalStorage("tasks", "tag_field")

        tag = "Bar"
        item = {"tasks": "foobar", "tag_field": tag}
        store.put_item(item)
        store.put_item({"tasks": "foobaz", "tag_field": "Baz"})

        get_items = store.get_items({"another_tag_field": tag})
        self.assertEqual(len(get_items), 0, "No item should have matched tag in invalid tag field.")
    
    def test_getMultipleItems_UsingSingleTag(self):
        store = CSVLocalStorage("tasks", "tag_field")

        tag = "Bar"
        item = {"tasks": "foobar", "tag_field": tag}
        store.put_item(item)
        store.put_item({"tasks": "foobaz", "tag_field": tag})

        get_items = store.get_items({"tag_field": tag})
        with self.subTest():
            self.assertEqual(len(get_items), 2, "Two items should have matched tag in tag field.")

    def test_getSingleItem_UsingMutipleTagFields(self):
        store = CSVLocalStorage("tasks", "tag_field1", "tag_field2")

        tag1 = "Foo"
        tag2 = "Bar"
        item = {"tasks": "foobar", "tag_field1": tag1, "tag_field2": tag2}
        store.put_item(item)
        store.put_item({"tasks": "foobaz", "tag_field1": tag1, "tag_field2": "Baz"})
        store.put_item({"tasks": "barbaz", "tag_field1": "Bar", "tag_field2": "Baz"})

        get_items = store.get_items({"tag_field1": tag1, "tag_field2": tag2})
        with self.subTest():
            self.assertEqual(len(get_items), 1, "Only one item should have matched tags in tag fields.")
        with self.subTest():
            self.assertEqual(get_items[0], item, "Item from get should be item with matching tags in tag fields")

    def test_getMultipleItems_UsingMutipleTagsInEachTagField(self):
        store = CSVLocalStorage("tasks", "tag_field1", "tag_field2")

        tag1 = "Foo"
        tag2 = "Bar"
        tag3 = "Baz"
        item = {"tasks": "foobar", "tag_field1": tag1, "tag_field2": tag2}
        store.put_item(item)
        store.put_item({"tasks": "foobaz", "tag_field1": tag1, "tag_field2": "Baz"})
        store.put_item({"tasks": "barbaz", "tag_field1": "Bar", "tag_field2": "Baz"})

        get_items = store.get_items({"tag_field1": [tag1, tag2], "tag_field2": [tag2, tag3]})
        with self.subTest():
            self.assertEqual(len(get_items), 3, "All three items should have matched tags in tag fields.")


if __name__ == '__main__':
    unittest.main(verbosity=2)