class CSVLocalStorage:
    """Provides storage to a local csv file

    """
    def __init__(self, *keys):
        self.keys = list(keys) if keys else ["item"]
        self.store = []

    def get_items(self, tags=None):
        if not tags:
            return self.store
        if not set(tags.keys()).issubset(set(self.keys)):
            return []
        return [item for item in self.store if self._tags_in(item, tags)]

    def put_item(self, item):
        if isinstance(item, dict) and list(item.keys()) == self.keys:
            self.store.append(item)

    def _tags_in(self, item, tags):
        for key, value in tags.items():
            if isinstance(value, list):
                matched_tags = [tag for tag in value if item[key] == tag]
                if not matched_tags:
                    return False
            else:
                if item[key] != value:
                    return False
        return True