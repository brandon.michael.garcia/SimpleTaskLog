from csvlocalstorage import CSVLocalStorage

def main():
    store = CSVLocalStorage("key")
    store.put_item({"key": "value"})
    print(f"{store.get_items()}")

if __name__ == '__main__':
    main()