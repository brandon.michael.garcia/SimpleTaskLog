Simple Task Log


Provides simple task logging to a locally stored csv file.

Each task has three required properties (timestamp, task, tags), but can be extended with additional encapsulated information.

